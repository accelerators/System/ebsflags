# EbsFlags

A Tango device exporting EBS flags (boolean atributes) to which clients can subscribe to be notified of machine wide events 
(e.g., TopUpInjectionInProgress)
