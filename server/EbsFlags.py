###################################################################################################
### Imports
###################################################################################################
import logging
import datetime
import threading
import collections
from typing import Any
#--------------------------------------------------------------------------------------------------
import tango
from tango.server import Device, device_property, command, attribute, run
#--------------------------------------------------------------------------------------------------

###################################################################################################
### EbsFlags
###################################################################################################
class EbsFlags(Device): 

    TopUpInjectionInProgressTmoInSeconds = device_property(dtype=tango.DevUShort, default_value=120)

    class TangoHandler(logging.Handler):

        def __init__(self, host_device: Device) -> None:
            logging.Handler.__init__(self)
            self.__host_device = host_device
            self.__logging_func = {}
            self.__logging_func[logging.CRITICAL] = host_device.get_logger().fatal
            self.__logging_func[logging.ERROR] = host_device.get_logger().error
            self.__logging_func[logging.WARNING] = host_device.get_logger().warn
            self.__logging_func[logging.INFO] = host_device.get_logger().info
            self.__logging_func[logging.DEBUG] = host_device.get_logger().debug
            self.__logging_func[logging.NOTSET] = lambda msg: None

        def emit(self, record: Any):
            self.__logging_func[record.levelno](self.format(record))

    class TracesHandler(logging.Handler):

        def __init__(self, host_device: Device, max_len: int) -> None:
            logging.Handler.__init__(self)
            self.__traces = collections.deque(maxlen=max_len)
            host_device.set_traces_buffer(self.__traces)

        def emit(self, record: Any) -> None:
            self.__traces.append(self.format(record))

    def __setup_logger(self, max_len=4096):
        lg = logging.getLogger("EbsFlags")
        lg.handlers.clear()
        trh = EbsFlags.TracesHandler(self, max_len)
        lf = logging.Formatter("%(asctime)s [%(threadName)4s] [%(levelname)7s]: %(message)s  ")
        trh.setFormatter(lf)
        lg.addHandler(trh)
        tgh = EbsFlags.TangoHandler(self)
        lf = logging.Formatter("[%(threadName)4s]: %(message)s  ")
        tgh.setFormatter(lf)
        lg.addHandler(tgh)
        lg.setLevel('DEBUG')
        return lg

    # the device initialization 
    def init_device(self):
        super().init_device()
        try:
            # rename the current thread for logging purpose
            t = threading.current_thread()
            t.name = "MAIN"
            # EBS flags
            self.__flags = {
                'TopUpInjectionInProgress': {'value':False, 
                                             'timeout':self.TopUpInjectionInProgressTmoInSeconds, 
                                             'timestamp':datetime.datetime.now()}
            }
            self.__injection_in_progress = False
            # fire change event by hand
            iip = self.get_device_attr().get_attr_by_name("TopUpInjectionInProgress")
            iip.set_change_event(True, False)
            iip.set_archive_event(True, False)
            # setup the device logger
            self.__logger = self.__setup_logger()
            # switch to our "up and running" state & status
            self.set_state(tango.DevState.ON)
            status = "EBS-Flags device is up and running"
            self.__logger.info(status)
            self.set_status(status)
        except Exception as e:
            # just an example
            self.__logger.exception(e)
            self.set_state(tango.DevState.FAULT)
            self.set_status("EbsFlags.init_device failed!")

    def delete_device(self):
        pass

    def always_executed_hook(self):
        # change thread name for logging purpose
        t = threading.current_thread()
        t.name = "TGCT"

    def set_traces_buffer(self, traces):
        self.__traces = traces
   
    @command(dtype_in=tango.DevVoid, dtype_out=tango.DevVoid)
    def ClearTraces(self) -> None:
        self.__traces.clear()

    @command(dtype_in=tango.DevVoid, dtype_out=tango.DevVoid)
    def CheckFlagsTimeout(self) -> None:
        for flag, flag_info in self.__flags.items():
            elapsed_seconds = (datetime.datetime.now() - flag_info['timestamp']).total_seconds() 
            if flag_info['value'] and elapsed_seconds >= flag_info['timeout']:
                self.__logger.info("timeout expires for flag '{}' - resetting it's value to False".format(flag))
                self.__flags[flag]['timestamp'] = datetime.datetime.now()
                self.__flags[flag]['value'] = False

    @attribute(dtype=[str], max_dim_x=4096)
    def Traces(self):
        return list(self.__traces) 

    @attribute(dtype=tango.DevBoolean)
    def TopUpInjectionInProgress(self):
        return self.__flags['TopUpInjectionInProgress']['value']

    @TopUpInjectionInProgress.write
    def TopUpInjectionInProgress(self, iip:bool):
        self.__flags['TopUpInjectionInProgress']['timestamp'] = datetime.datetime.now()
        self.__flags['TopUpInjectionInProgress']['value'] = iip
        self.push_change_event("TopUpInjectionInProgress", iip);
        self.push_archive_event("TopUpInjectionInProgress", iip);
        self.__logger.info("flag TopUpInjectionInProgress set to {}".format(iip))
    
if __name__ == '__main__':
    try:
        run((EbsFlags,))
    except Exception as e:
        print(e)
        raise
